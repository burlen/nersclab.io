# CVMFS

[CVMFS (CernVM-File System)](https://cvmfs.readthedocs.io/en/stable/)
is a software distribution service heavily used by High-Energy Physics
(HEP) experiments to deliver software to multiple different computing
systems worldwide.  It presents remote software pulled over http as a
POSIX read-only file system in user space (normally using FUSE).
Access is performant and scalable due to multiple levels of caching
such as a local file cache and squid web caches.

## CVMFS at NERSC

The CVMFS filesystem is accesible at `/cvmfs` and can be accessed from 
any login node or compute node. The current list of supported 
repositoitories is listed below. If you need a repository added please 
reach out at [help.nersc.gov](https://help.nersc.gov).

    nersc$ ls /cvmfs
    alice.cern.ch
    alice-ocdb.cern.ch
    ams.cern.ch
    atlas.cern.ch
    atlas-condb.cern.ch
    atlas-nightlies.cern.ch
    cernvm-prod.cern.ch
    cms.cern.ch
    config-osg.opensciencegrid.org
    cvmfs-config.cern.ch
    dunedaq.opensciencegrid.org
    dune.opensciencegrid.org
    fermilab.opensciencegrid.org
    gm2.opensciencegrid.org
    grid.cern.ch
    icecube.opensciencegrid.org
    larsoft.opensciencegrid.org
    lhcb.cern.ch
    lhcb-condb.cern.ch
    lz.opensciencegrid.org
    minerva.opensciencegrid.org
    mu2e.opensciencegrid.org
    nova-development.opensciencegrid.org
    nova.opensciencegrid.org
    oasis.opensciencegrid.org
    sft.cern.ch
    sft-nightlies.cern.ch
    spt.opensciencegrid.org
    sw.lsst.eu
    uboone.opensciencegrid.org
    unpacked.cern.ch

## CVMFS in Containers

If using [shifter](../development/shifter/how-to-use.md) you should
specify the `--modules=cvmfs` flag to shifter to make that mount
appear inside your container (unless you want to use your own version
of cvmfs inside the container).

    nersc$ shifter --module=cvmfs

You can also use the new container runtime 
[podman-hpc](../development/podman-hpc/overview.md) with CVMFS by
mounting the `/cvmfs` directory into the container.

    nersc$ podman-hpc run --rm -it -v /cvmfs:/cvmfs container:tag 

## CVMFS Batch Jobs

You can also load this module in your batch script and you should also
add the cvmfs file system license `-L cvmfs` which would allow us to
pause your jobs from running if the cvmfs mount was unavailable

    #!/bin/bash
    #SBATCH --module=cvmfs
    #SBATCH -L cvmfs
