# Rancher 2.5 to Rancher 2.6 Upgrade

NERSC upgraded from Rancher 2.5 to 2.6 in Spring 2023, which includes a
major update to the graphical user interface. The new UI is named
"Cluster Explorer".

This page contains a summary of major changes to the UI, and also a list of
known UI bugs.

## Summary of changes between Rancher 2.5 and Rancher 2.6

| Feature            | Rancher 2.5                                           | Rancher 2.6 |
| -------            | -----------                                           | ----------- |
| Overall navigation | Resources located in various menus                    | [All resources are located under the left hand navigation bar](#new-ui-navigation) |
| Overall navigation | Resources located in various menus                    | [Resource options are located within the resource context](#workload-navigation-menu) |
| Shell & log window | Shell window was a full screen                        | [Shell access & logs open multiple windows side-by-side](#shell-access-and-logs) |
| Namespaces         | Could create namespace while creating workload        | [Must create namespace before workload](#must-create-namespace-before-workload) |
| Workload creation  | Can paste multiple key-value pairs as environment variables | [Config Maps are recommended over environment variables.](#config-maps)<br>You can paste multiple key-value pairs as Config Maps.<br>During workload creation, environment variables do not support the bulk paste of multiple key-value pairs. |
| Workload Creation  | Security Context is at bottom of workload config page | [Security context is found under Container-0](#security-context) |
| Secrets            | Secrets, Certs, Registry Credentials are in separate menus under the ‘Secrets’ menu | [Secrets are located under Storage](#secrets). The default type of secret is “Opaque” |
| Ingresses          | Ingresses were located under *Workload* > *Loadbalancers*  | [Ingresses are located under *Service Discovery*](#ingresses) |
| Loadbalancers      | Loadbalancers were created from a workload            | [Loadbalancers are located under *Service Discovery* > *Services*](#loadbalancers) |
| Volumes            | Volumes were located under *Workload* > *Volumes*     | [Volumes are located under *Storage* > *PersistentVolumeClaims*](#volumes) |

### New UI navigation

![Rancher 2.6 Navigation Menu](Images/R26-navigation.png){style="border:1px solid black"}

Here are a few highlights of the new UI:

* The new navigation menu on the left side contains all resources that are
  available to you. Here are the location of some important resource types.
    * Namespaces are listed under the `Cluster` menu.
    * Deployments are listed under the `Workloads` menu
    * Ingresses and Loadbalancers are listed under the `Service Discovery`
      menu
* Notice the magnifying glass in the upper right hand corner. This allows
  you to search for resources by their type, such as a Deployment or Pod.
* If you work with multiple namespaces, you can select which namespaces to
  view in the namespace dropdown above. 
    ![Multiple namespaces can be selected](Images/R26-select-multiple-namespaces.png){style="border:1px solid black"}
* We recommend that you stay away from the menu titled "More Resources"
  or the button titled "Cluster Tools". The items contained within tend to
  be confusing to most folks. In addition, you will not have permission to use
  some of the things and will get an error message if you try to activate
  them. **Here be Dragons**

### Workload navigation menu

Similar to the left hand navigation menu, items in the workload navigation
menus are organized by a hierarchy. In the example below, we can see that a
`Deployment` contains the `Pod`, and the `Pod` contains the `Container`.

![Rancher 2.6 Deployment Menu](Images/R26-deployment-navmenu.png){style="border:1px solid black"}

### Shell access and logs

Opening a shell or log can be done from the dropdown menu next to a Pod.
This will open an in-browser window as shown by the arrows below. A few
things to notice:

* The shell and log windows will time out after a few minutes of inactivity.
  Notice the green word "Connected" in the bottom right corner. If it says
  "Disconnected" in red, your session has been disconnected or the container
  has stopped.
* The window bar across the top of the log or shell window has an
  "expansion icon" which allows you to minimize or expand the windows.

![Accessing logs and the shell in Rancher 2.6](Images/R26-shell-logs.png){style="border:1px solid black"}

### Containers are now launched without the `--interactive` and `--tty` flags

Note that containers are now launched without the `--interactive` and `--tty`
flags. As a consequence, containers without a long running entrypoint will
exit immediately and you will not be able to open an interactive shell. One
notable example is the `ubuntu` series of images.

You can re-enable these features in the container configuration. Navigate to
the container tab, scroll down to the `Command` section and select `Yes` for
`Stdin` and check the box next to `TTY` as shown here:

![Enabling interactive shell and tty in Rancher 2.6](Images/R26-shell-interactive-tty.png){style="border:1px solid black"}

### Must create namespace before workload

Rancher 2.5 allowed you to create a namespace when creating a workload:

![Creating namespaces in Rancher 2.5\
](Images/R25-create-namespace.png "Creating namespaces in Rancher 2.5"){style="border:1px solid black"}

In Rancher 2.6, namespaces must be created before creating the workload.
Namespaces are created from the top left menu, `Cluster` > `Namespaces` >
`Create Namespace`, as shown here:

![Creating namespaces in Rancher 2.6\
](Images/R26-create-namespace.png "Creating namespaces in Rancher 2.6"){style="border:1px solid black"}

Note that *Projects* may only be created by NERSC staff. 

### Config Maps

In Rancher 2.6, the recommended practice for adding environment variables is
to use Config Maps. A user can paste multiple key-values into Config Maps at
once, and then use those Config Maps in a deployment. The Rancher 2.6 UI
does not allow a user to bulk paste multiple key-value pairs into the
`Environment Variables` section during workload creation.

![Pasting multiple key-value pairs into Config Maps](Images/R26-configmap-bulk-paste.png){style="border:1px solid black"}
![Using Config Maps as environment variables](Images/R26-configmap-as-environment-variables.png){style="border:1px solid black"}

### Security context

In Rancher 2.5, the security context for the workloads was available at the
far bottom of the page, after clicking `Advanced Options`:

![Security Context in Rancher 2.5](Images/R25-security-context.png "Security Context in Rancher 2.5"){style="border:1px solid black"}

In Rancher 2.6, the security context is located under the "Container" tab,
as shown here:

![Security Context in Rancher 2.6](Images/R26-security-context.png "Security Context in Rancher 2.6"){style="border:1px solid black"}

### Secrets

In Kubernetes, secrets are listed under the `Storage` menu because a secret
is technically a storage object. The default type of secret is an *Opaque*
secret-- this was true in Rancher 2.5 as well, but the UI simply called them
"Secrets" and didn't use the word "Opaque".

![Secrets are located under Storage](Images/R26-secrets.png){style="border:1px solid black"}

### Ingresses

Ingresses are now located under *Service Discovery*

![Ingresses are located under Service Discovery](Images/R26-ingresses.png){style="border:1px solid black"}

### Loadbalancers

Loadbalancers are now located under *Service Discovery* > *Services*

![Loadbalancers are located under Service Discovery > Services](Images/R26-loadbalancers.png){style="border:1px solid black"}

### Volumes

Volumes have been renamed to use the Kubernetes term "Persistent Volume
Claim" (PVC) and can be found under *Storage* > *PersistentVolumeClaims*

![Volumes are located under Storage](Images/R26-volumes.png){style="border:1px solid black"}

<!--- Re-enable when we find actual bugs
## Known Bugs
-->
