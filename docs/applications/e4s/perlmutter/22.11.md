# E4S 22.11 - Perlmutter

The `e4s-22.11` stack is built using the Spack branch [e4s-22.11](https://github.com/spack/spack/tree/e4s-22.11)
on Perlmutter using the *gcc*, *nvhpc*, and *cce* compilers. This stack can be loaded by running:

```
module load e4s/22.11
```

**You can also load the same software stack via `module load spack/e4s-22.11`**

Here is a breakdown of the installed specs by environments:

|  Spack Environments | Compiler           | Root Specs | Implicit Specs | Total Specs |
|---------------------|--------------------|------------|----------------|-------------|
| `gcc`               | `gcc@11.2.0`       | 85         | 313            | 398         |
| `cuda`              | `gcc@11.2.0`       | 27         | 72             | 99          |
| `nvhpc`             | `nvhpc@22.5`       | 5          | 7              | 12          |
| `cce`               | `cce@15.0.0`       | 9          | 13             | 22          |

## Spack Configuration

When you load the `e4s/22.11` module you will see several environments with their own isolated software stacks. 
You can list the environments by running the following: 

```shell
elvis@perlmutter> spack env list
==> 4 environments
    cce  cuda  gcc  nvhpc
```

To activate an environment, run the following:

```shell
elvis@perlmutter> spack env activate gcc
```

## Overview

You can see a list of all available packages via `spack find`. For example, 
to see the packages within the `gcc` environment:

```shell
spack env activate gcc
spack find
```

Using the `-e` flag, you can list packages without activating an environment.

```shell
spack -e gcc find 
```

To format the output of `spack find`, use the ``--format`` flag. Shown below is a command 
to see a list of packages:

```shell
spack -e gcc find -x --format "{name}@{version}%{compiler} {variants} /{hash:7}
```

A few useful `spack find` commands for querying packages:

```shell
# list all hdf5 packages with full hashes, variants, prefix, and dependencies
spack -e gcc find -Lvpd hdf5

# list all root (explicit) specs
spack -e gcc find -x

# list all implicit (dependencies) specs
spack -e gcc find -X

# list all root specs dependent on mpi
spack -e gcc find -x ^mpi
```
