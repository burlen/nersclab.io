# List of publications using WRF by NERSC users 

[//]: # (comment: use "lazy numbering" for a long, changing list - markdown automatically numbers each item)
[//]: # (comment: it would be nice if we can use google form that a user can input required data, which is then forwarded to a SIG member by email)

1. Chan, M.-Y., Chen X. and Anderson J. L. (2023). The potential benefits of handling clear and cloudy ensemble
 members separately through an efficient bi-Gaussian EnKF. Journal of Advances in Modeling Earth Systems. 
 doi: 10.1029/2022MS003357

1. Chen, X., Leung, L. R., Gao, Y., Liu, Y., Wigmosta, M., & Richmond, M. (2018). 
Predictability of Extreme Precipitation in Western U.S. Watersheds Based on Atmospheric River Occurrence,
 Intensity, and Duration. Geophysical Research Letters 
(Vol. 45, Issue 21, pp. 11,693-11,701). https://doi.org/10.1029/2018GL079831

1. Ovchinnikov, M., Fast, J. D., Berg, L. K., Gustafson, W. I., Chen, J., Sakaguchi, K., & Xiao, H. (2022). 
 Effects of Horizontal
 Resolution, Domain Size, Boundary Conditions, and Surface Heterogeneity on Coarse LES of a Convective Boundary Layer. 
 Monthly Weather Review, 1397–1415. https://doi.org/10.1175/mwr-d-21-0244.1

1. Sakaguchi, K., Berg, L. K., Chen, J., Fast, J., Newsom, R., Tai, S. ‐L., Yang, Z., Gustafson, W. I., Gaudet, B. J., Huang, M., 
Pekour, M., Pressel, K., & Xiao, H. (2021). Determining Spatial Scales of Soil Moisture – Cloud Coupling Pathways using 
Semi‐Idealized Simulations. Journal of Geophysical Research: Atmospheres, May 2021. https://doi.org/10.1029/2021jd035282

1. Hagos S.M., J. Chen, K.A. Barber, K. Sakaguchi, R.S. Plant, Z. Feng, and H. Xiao. (2022). 
A Machine-Learning-Assisted Stochastic Cloud Population Model as a Parameterization of Cumulus Convection.
Journal of Advances in Modeling Earth Systems 14, no. 7:e2021MS002808. doi:10.1029/2021MS002808

1. Chan, M.-Y., X. Chen, and L.R. Leung. 2022. “A High-Resolution Tropical Mesoscale Convective System Reanalysis (TMeCSR) 
Product.” J. Adv. Model. Earth Syst., 14, doi:10.1029/2021MS002948.

1. Hu, Z., C. Zhao, L.R. Leung, Q. Du, Y. Ma, S. Hagos, Y. Qian, and W. Dong. 2022. “Characterizing the Impacts of Atmospheric 
Rivers on Aerosols in the Western U.S.” Geophys. Res. Lett., 49, doi:10.1029/2021GL096421.

1. Chen, X., L.R. Leung, Z. Feng, and Q. Yang. 2022. “Precipitation-Moisture Coupling Over Tropical Oceans: Sequential roles of 
Shallow, Deep, and Mesoscale Convective Systems.” Geophys. Res. Lett., 49, doi:10/1029/2022GL097836.

1. Li, J., Y. Qian, L.R. Leung, Z. Feng, C. Sarangi, Y. Liu, and Z. Yang. 2022. “Impacts of Large-Scale Urbanization and 
Irrigation on Summer Precipitation in the Mid-Atlantic Region.” Geophys. Res. Lett., 49, doi:10.1029/2022GL097845.

1. Chen, X., L.R. Leung, Z. Feng, and F. Song. 2022. “Crucial Role of Mesoscale Convective Systems in the Vertical Mass, Water, 
and Energy Transport of the South Asian Summer Monsoon.” J. Clim., 35, 91-108, doi:10.1175/JCLI-D-21-0124.1.

1. Chen, X., L.R. Leung, Z. Feng, F. Song, and Q. Yang. 2021. “Mesoscale Convective Systems Dominate the Energetics of the South 
Asian Summer Monsoon Onset.” Geophys. Res. Lett., 48, doi:10.1029/2021GL094873.

1. Chen, X., L.R. Leung, Y. Gao, and Y. Liu. 2021. “Response of U.S. West Coast Mountain Snowpack to Local SST Perturbations: 
Insights from Numerical Modeling and Machine Learning Models.” J. Hydrometeor., 22, 1045-1062, doi:10.1175/JHM-D-20-0127.1.

1. Balaguru, K., L.R. Leung, S.M. Hagos, and K. Sujith. 2021. “An Oceanic Pathway for Madden-Julian Oscillation Influence on 
Maritime Continent Tropical Cyclones.” NPJ Clim. Atmos. Sci., 4:52, doi:10.1038/s41612-021-00208-4.

1. Chen, X., and L.R. Leung. 2020. “Response of Landfalling Atmospheric Rivers on the U.S. West Coast to Local Sea Surface 
Temperature Perturbations.” Geophys. Res. Lett., 47, e2020GL089254, doi:10.1029/2020GL089254.

1. Chen, X., Z. Duan, L.R. Leung, and M. Wigmosta. 2019. “A Framework to Delineate Precipitation-Runoff Regimes: Precipitation 
vs. Snowpack in the Western U.S.” Geophys. Res. Lett., 46, doi:10.1029/2019GL085184.

1. Chen, X., L.R. Leung, M. Wigmosta, and M. Richmond. 2019. “Impact of Atmospheric Rivers on Surface Hydrological Processes in 
Western U.S. Watersheds.” J. Geophys. Res., 124, doi:10.1029/2019JD030468.
 